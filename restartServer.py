#! /usr/bin/python
#　-*- coding: utf-8 -*-

import os
import sys
import signal
import string 
import psutil
# from pprint import pprint as pp

def get_pid(cmdName, para):
    pid = 0
    for p in psutil.process_iter(attrs=['pid', 'cmdline']):
        cmdLine = p.info['cmdline']
        if cmdLine:
            if len(cmdLine) == 2:
                if (cmdName in cmdLine[0]) and (para in cmdLine[1]):
                    pid = p.info['pid']
                    break
    return pid

def kill(pid):

    try:
        a = os.kill(pid, signal.SIGKILL)
    except OSError, e:
        pass
if __name__ == '__main__':
    try:
        taskPid = get_pid('node', 'index.js')
        print(taskPid)
        if taskPid > 0:
            kill(taskPid)
        os.system('node ./index.js &')
        # os.system('node /var/tokyweb/index.js &')
    except:
        print("some errors.")
    else:
        pass