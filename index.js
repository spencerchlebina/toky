const LOCATION = 'SERVER'  //  LOCAL or  SERVER

const STAND_DIR = __dirname + '/public_b'
const BETA_DIR = __dirname + '/public'

const ROUTER_BETA = '/earlybird'
// 

var express = require('express');
var fs = require('fs');
var app = express();
var path = require('path');
var exec = require('child_process').exec;
var execSync = require('child_process').execSync;

var forceSSL = require('express-force-ssl');
var morgan = require('morgan')

const https = require('https');

var USER_DIR;
var USER_CODE_DIR;
var PORT_NUM = 3000;



// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})

// setup the logger
app.use(morgan('short', {
    skip: function (req, res) { return res.statusCode < 400 },
    stream: accessLogStream
}))


app.set('port', (process.env.PORT || PORT_NUM));
/*
if ('SERVER' == LOCATION)
{
  PORT_NUM = 80

  app.use(forceSSL);
}
else
{
  PORT_NUM = 8080
}
*/
// set router
app.use('/', express.static(STAND_DIR));
app.use(ROUTER_BETA, express.static(BETA_DIR));

app.use(require('helmet')());
//Changes Below
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Pointing to the config directory
//Including modules
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
//Including API routes
const users = require('./public_b/routes/users');
const auth = require('./public_b/routes/auth');
const clicks = require('./public_b/routes/clicks');
const projects = require('./public_b/routes/projects');

mongoose.connect('mongodb://localhost/toky')
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.log('Could not connect to MongoDB...')); //Connecting to MongoDB using monggose or sending an error message

app.use(bodyParser.json()); //bodyParser.json() is used to parse json objects sent in request bodies
//Setting up API endpoints
app.use('/api/users', users); 
app.use('/api/auth', auth);
app.use('/api/clicks', clicks);
app.use('/api/projects', projects);

//Changes Above
app.get('/sendCode/', function(request, response) {
	try{
        var USER_DIR;
        var USER_CODE_DIR;
        var INCLUDE_FILE

        var referer = request.headers.referer
        if ( referer.search(ROUTER_BETA) > 1 )
        {
          USER_DIR =  BETA_DIR + '/user/'
          INCLUDE_FILE = '#include "gen_extern.h" \n'
        }
        else
        {
          USER_DIR = STAND_DIR + '/user/'
          INCLUDE_FILE = '#include "gen_extern.h" \n'
        }

        USER_CODE_DIR = USER_DIR + 'code/'

        var code = request.query.code;
	    //console.log(code);
        var random_hexa = Math.random().toString(16).substr(2);  //todo 
        //var hash = code.hashCode();
        // console.log("send " + random_hexa);           

        execSync("mkdir -p " + USER_CODE_DIR + random_hexa);
        code = INCLUDE_FILE + code;
 		fs.writeFileSync(USER_CODE_DIR + random_hexa + "/" + "userfunc.cpp", code);
 		execSync("python " + USER_DIR + "makepatch.py " + random_hexa);

		response.send(random_hexa);

	}
	catch(e){
		console.log(e);
		response.sendStatus(201);

	}
});	

app.get('/getHex/*', function(request, response) {
	try{
        var USER_DIR;
        var USER_CODE_DIR;

        var referer = request.headers.referer
        if ( referer.search(ROUTER_BETA) > 1 )
        {
          USER_DIR =  BETA_DIR + '/user/'
        }
        else
        {
          USER_DIR = STAND_DIR + '/user/'
        }

        USER_CODE_DIR = USER_DIR + 'code/'

        var random_hexa = request.url.split("/")[2];
        var patchDir = USER_CODE_DIR + random_hexa;

        response.download(patchDir + "/patch.bin", function(err){
            if(err){
                console.log(err);
                response.sendStatus(400);
      
            }
            else{
                deleteall(patchDir);
            }

        });

	}
	catch(e){
		console.log(e);
		response.sendStatus(401);
	}
});


app.get('/getfirmware/*', function(request, response) {
    try{
        var url = request.url.split("/");
        var version;
        var fileName;
        var num;
        var fileDir;

        num = url.length;

        fileName = url[num-1];
        version = url[num-2];

        if ( 'beta' == version )
        {
          fileDir = BETA_DIR + '/firmware/';
        }
        else
        {
          fileDir = STAND_DIR + '/firmware/';
        }


        var fileDst = fileDir+fileName;

        var stat = fs.statSync(fileDst)

        response.writeHead(200, { 'Content-Type': 'bin', 'Content-Length': stat.size})
        var readStream = fs.createReadStream(fileDst);
        readStream.pipe(response);
    }
    catch(e){
        console.log(e);
        response.sendStatus(204);
    }
});

app.listen(PORT_NUM);

if ('SERVER' == LOCATION)
{
    const options = {
    // cert: fs.readFileSync(__dirname + '/sslcert/fullchain.pem'),
    // key: fs.readFileSync(__dirname + '/sslcert/privkey.pem')
    cert: fs.readFileSync(__dirname + '/sslcert/createinchina/Nginx/1_createinchina.tokylabs.com_bundle.crt'),
    key: fs.readFileSync(__dirname + '/sslcert/createinchina/Nginx/2_createinchina.tokylabs.com.key')
    };

    https.createServer(options, app).listen(443);
}

process.on('uncaughtException', function (err) {
  console.log(err);
});


function deleteall(path) {  
    var files = [];  
    if(fs.existsSync(path)) {  
        files = fs.readdirSync(path);  
        files.forEach(function(file, index) {  
            var curPath = path + "/" + file;  
            if(fs.statSync(curPath).isDirectory()) { // recurse  
                deleteall(curPath);  
            } else { // delete file  
                fs.unlinkSync(curPath);  
            }  
        });  
        fs.rmdirSync(path);  
    }  
};  


