/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2017 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Example "wait" block that will pause the interpreter for a
 * number of seconds. Because wait is a blocking behavior, such blocks will
 * only work in interpreted environments.
 *
 * See https://neil.fraser.name/software/JS-Interpreter/docs.html
 */

/**from lofi**/

const FRAME_END = 0xFA;
const FUNC_SETOUTPUT = 0x80;
const FUNC_SCR_CLEAR = 0x81;
const FUNC_SCR_PRINT = 0x82;
const FUNC_SCR_DRAWPIXEL =0x83;


/******/ 

Blockly.Blocks['main_repeat'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("repeat")
        .appendField(new Blockly.FieldImage("media/loop_icon.svg", 30, 30, "*"));
    this.appendStatementInput("repeat_code")
        .setCheck(null);
    this.setColour(197);
    this.setTooltip('repeat forever');
    this.setHelpUrl('');
    // // this.setDeletable(false);
  }
};

Blockly.CCode['main_repeat'] = function(block) {
  var statements_repeat_code = Blockly.JavaScript.statementToCode(block, 'repeat_code');
  // TODO: Assemble JavaScript into code variable.

  var code = "while(1){\n"
  code += statements_repeat_code;
  code += "}\n"

  return code;
};

Blockly.JavaScript['main_repeat'] = function(block) {
  var statements_repeat_code = Blockly.JavaScript.statementToCode(block, 'repeat_code');
  // TODO: Assemble JavaScript into code variable.

  var code = "while(1){\n"
  code += statements_repeat_code;
  code += "}\n"

  return code;
};


Blockly.Blocks['delay_block'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Wait");
    this.appendValueInput("delay_time")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("ms");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(197);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};



Blockly.JavaScript['delay_block'] = function(block) {
  var value_delay_time = Blockly.JavaScript.valueToCode(block, 'delay_time', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.

  if (value_delay_time == "") {

    value_delay_time = "0";
  }

  var code = 'delay('+ value_delay_time +');\n';
  return code;
};


/*****/


Blockly.Blocks['read_input'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read")
        .appendField(new Blockly.FieldDropdown([["INPUT1",0], ["INPUT2",1], ["INPUT3",2]]), "input_number");
    this.setOutput(true, "Number");
    this.setColour(17);
    this.setTooltip('read value from INPUT');
    this.setHelpUrl('');
  }
};


Blockly.JavaScript['read_input'] = function(block) {
  var dropdown_input_number = block.getFieldValue('input_number');

  var code = 'getInput(' + dropdown_input_number + ')';

  // if (dropdown_input_number == 'input1') {
  //   code = "analogRead0";
  // }
  // if (dropdown_input_number == 'input2') {
  //   code = "analogRead1";
  // }
  // if (dropdown_input_number == 'input3') {
  //   code = "analogRead2";
  // }

  //var code = "analogRead0";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.Blocks['output'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Set output")
        .appendField(new Blockly.FieldDropdown([["OUTPUT1",0], ["OUTPUT2",1], ["OUTPUT3",2]]), "output_number")
        .appendField("to");
    this.appendValueInput("power")
        .setCheck("Number")        
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(17);
    this.setTooltip('Set value to OUTPUT');
    this.setHelpUrl('');
  }
};


Blockly.JavaScript['output'] = function(block) {
  var dropdown_output_number = block.getFieldValue('output_number');
  var value_power = Blockly.JavaScript.valueToCode(block, 'power', Blockly.JavaScript.ORDER_ATOMIC);

  if (value_power == "")
    value_power = "0";

  var code = 'setOutput(' + dropdown_output_number + ',' + value_power +');\n';
  // var code = 'setOutput("' + dropdown_output_number + '","' + value_power +'");\n';

  return code;
};


Blockly.Blocks['print_screen'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Print to Screen");
    this.appendValueInput("content")
        .setCheck("String");
    this.appendDummyInput()
        .appendField("at row=");
    this.appendValueInput("row")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("column");
    this.appendValueInput("column")
        .setCheck("Number");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(17);
 this.setTooltip("Print string to screen");
 this.setHelpUrl("");
  }
};


Blockly.JavaScript['print_screen'] = function(block) {
  var value_content = Blockly.JavaScript.valueToCode(block, 'content', Blockly.JavaScript.ORDER_ATOMIC);
  var value_row = Blockly.JavaScript.valueToCode(block, 'row', Blockly.JavaScript.ORDER_ATOMIC);
  var value_column = Blockly.JavaScript.valueToCode(block, 'column', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'Screen_print(' + value_column + ',' + value_row + ',' + value_content +');\n'
  return code;
};

Blockly.Blocks['drawPixel_screen'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Draw pixel at row=");
    this.appendValueInput("row")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("column");
    this.appendValueInput("column")
        .setCheck("Number");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(17);
    this.setTooltip("Draw pixel on screen");
    this.setHelpUrl("");
  }
};

Blockly.JavaScript['drawPixel_screen'] = function(block) {
  var value_row = Blockly.JavaScript.valueToCode(block, 'row', Blockly.JavaScript.ORDER_ATOMIC);
  var value_column = Blockly.JavaScript.valueToCode(block, 'column', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'Screen_drawPixel(' + value_column + ',' + value_row +');\n'
  return code;
};

Blockly.Blocks['clear_screen'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Clear Screen");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Clear content on screen");
 this.setHelpUrl("");
  }
};

Blockly.JavaScript['clear_screen'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'Screen_clear();\n';
  return code;
};







//https://neil.fraser.name/software/JS-Interpreter/docs.html
/**
 * Register the interpreter asynchronous function
 * <code>waitForSeconds()</code>.
 */
function initInterpreterDelay(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('delay');

  var wrapper = interpreter.createAsyncFunction(
    function(milliseconds, callback) {
      // Delay the call to the callback.
      setTimeout(callback, milliseconds);
    });
  interpreter.setProperty(scope, 'delay', wrapper);
}
/**
 * Register the interpreter asynchronous function
 * <code>setOutputPWM()</code>.
 */
function initInterpreterSetOutputPWM(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('setOutputPWM');
  var wrapper = interpreter.createAsyncFunction(
    function(pinNumber, PWMDutyCycle, callback) {
      // Delay the call to the callback.
      //setTimeout(callback, timeInSeconds * 1000);
      //let text = encoder.encode("D" + byte(pinNumber) + "," + PWMDutyCycle + ";");
      var bufView = new Uint8Array(8);
      bufView[0] = 0x0e;
      bufView[1] = pinNumber;
      bufView[2] = PWMDutyCycle;
      //console.log("Sending S" + pinNumber + "," + PWMDutyCycle + ";" + '\u000A\u000D');
      window.bluetoothCaracteristic.writeValue(bufView)
      .then(()=>callback())
      .catch(err=>console.error(err));
      });
  interpreter.setProperty(scope, 'setOutputPWM', wrapper);
}


/**
 * Register the interpreter asynchronous function
 * <code>getBLSensorValue()</code>.
 */
function initInterpreterGetBLSensorValue(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('getBLSensorValue');

  var wrapper = interpreter.createAsyncFunction(
    function(pinNumber, callback) {
        window.bluetoothCaracteristic.readValue()   //read data from esp ; leoYan
        .then(value => {
          var arr = new Uint8Array(value.buffer)
          //console.log(arr);  
          console.log(arr[pinNumber]*255 + arr[pinNumber+1]);
          callback(arr[pinNumber]*255 + arr[pinNumber+1]);
        })
        .catch(err=>console.error(err));
      });
  interpreter.setProperty(scope, 'getBLSensorValue', wrapper);
}


function initInterpreterGetInput(interpreter, scope) {
 // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('getInput');

  var wrapper = interpreter.createAsyncFunction(
    function(pinIndex, callback) {
        window.bluetoothCaracteristic.readValue()   //read data from esp ; leoYan
        .then(value => {
          var Data = new Uint8Array(value.buffer);
          var index = Number(pinIndex);
          callback(Data[index]);
        })
        .catch(err=>console.error(err));
      });

  interpreter.setProperty(scope, 'getInput', wrapper);

}


function initInterpreterSetOutput(interpreter, scope) {
 // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('setOutput');
  var wrapper = interpreter.createAsyncFunction(
    function(pinIndex, duty, callback) {

      var Data = {para1:Number(pinIndex), para2: Number(duty)}

      var FrameData = makeTxFrame(FUNC_SETOUTPUT, Data);

      //console.log("Sending S" + pinNumber + "," + PWMDutyCycle + ";" + '\u000A\u000D');
      sendData(FrameData, callback);

  });

  interpreter.setProperty(scope, 'setOutput', wrapper);
}


function initInterpreterScreenPrint(interpreter, scope) {
 // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('Screen_print');
  var wrapper = interpreter.createAsyncFunction(
    function(x, y, str, callback) {
      var Data = {para1:Number(x), para2: Number(y), para3:str}

      var FrameData = makeTxFrame(FUNC_SCR_PRINT, Data);

      sendData(FrameData, callback);
  });

  interpreter.setProperty(scope, 'Screen_print', wrapper);
}

function initInterpreterScreenClear(interpreter, scope) {
 // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('Screen_clear');
  var wrapper = interpreter.createAsyncFunction(
    function(callback) {
      var Data = {}

      var FrameData = makeTxFrame(FUNC_SCR_CLEAR, Data);

      sendData(FrameData, callback);
  });

  interpreter.setProperty(scope, 'Screen_clear', wrapper);
}

function initInterpreterScreenDrawPixel(interpreter, scope) {
 // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('Screen_drawPixel');
  var wrapper = interpreter.createAsyncFunction(
    function(x, y, callback) {
      var Data = {para1:Number(x), para2: Number(y)}

      var FrameData = makeTxFrame(FUNC_SCR_DRAWPIXEL, Data);

      sendData(FrameData, callback);
  });

  interpreter.setProperty(scope, 'Screen_drawPixel', wrapper);
}

/*******/
function sendData( frameData, callback ){
  if (window.bluetoothCaracteristic)
  {
    window.bluetoothCaracteristic.writeValue(frameData)
        .then(()=>callback())
        .catch(err=>console.error(err));
  }
}

function makeTxFrame(functionId, dataCollector){

  Frame = new Array();
  Frame.push(functionId);

  for( item in dataCollector ){
    var itemData = dataCollector[item];
    var itemIype = typeof(itemData);

    switch(itemIype)
    {
    case 'number':
      Frame.push(itemData);

      break;
    case 'string':
      Frame.push( itemData.length );  

      for (var i = 0; i < itemData.length; i++ ) {  
        ch = itemData.charCodeAt(i);  // get char  
        Frame.push( ch );  
      }
      break;
    default:
    }

  }

  Frame.push(FRAME_END);

  console.log(Frame);

  return new Uint8Array(Frame);

} 






