#!/usr/bin/env python

import argparse
import os
import sys
import commands
import struct
import logging
import datetime

# from idf: esp_image_format.h  -> esp_image_header_t + esp_image_segment_header_t  + segment_data

# /* Main header of binary image */
# typedef struct {
#     uint8_t magic;
#     uint8_t segment_count;
#     /* flash read mode (esp_image_spi_mode_t as uint8_t) */
#     uint8_t spi_mode;
#     /* flash frequency (esp_image_spi_freq_t as uint8_t) */
#     uint8_t spi_speed: 4;
#     /* flash chip size (esp_image_flash_size_t as uint8_t) */
#     uint8_t spi_size: 4;
#     uint32_t entry_addr;
#     /* WP pin when SPI pins set via efuse (read by ROM bootloader, the IDF bootloader uses software to configure the WP
#      * pin and sets this field to 0xEE=disabled) */
#     uint8_t wp_pin;
#     /* Drive settings for the SPI flash pins (read by ROM bootloader) */
#     uint8_t spi_pin_drv[3];
#     /* Reserved bytes in ESP32 additional header space, currently unused */
#     uint8_t reserved[11];
#     /* If 1, a SHA256 digest "simple hash" (of the entire image) is appended after the checksum. Included in image length. This digest
#      * is separate to secure boot and only used for detecting corruption. For secure boot signed images, the signature
#      * is appended after this (and the simple hash is included in the signed data). */
#     uint8_t hash_appended;
# } __attribute__((packed))  esp_image_header_t;    //sizeof = 24

# typedef struct {
#     uint32_t load_addr;
#     uint32_t data_len;
# } esp_image_segment_header_t;


RAW_BIN_FILE = 'patch-raw.bin'
BIN_FILE = 'patch.bin'

RAW_HEARDER_LEN = 24
SEGMENT_HEAD_LEN = 8

SEGMENT_OFFSET = RAW_HEARDER_LEN
SEGMENT_DATA_OFFSET = (SEGMENT_OFFSET + SEGMENT_HEAD_LEN)

#outFile is little-endian
def postFile(inFile, outFile):
    fo = open(inFile, "rb")
    FileBuf = fo.read();
    fo.close()

    SegmentBuff = FileBuf[SEGMENT_OFFSET:] 

    s = struct.Struct('<2L')
    SegmentHead = s.unpack(SegmentBuff[:SEGMENT_HEAD_LEN])

    # print SegmentHead

    segmentDataLen = SegmentHead[1]
    #adjust align 4, the file is longer than code
    value = (segmentDataLen % 4)
    if 0 == value:
        remainder = 0
    else:
        remainder = 4 - value 

    segmentDataLen += remainder
    startIndex = SEGMENT_HEAD_LEN
    endIndex = startIndex + segmentDataLen

    # print 'remainder=%u, startIndex=%u, endIndex=%u' %(remainder, startIndex, endIndex)

    DataBuff = SegmentBuff[startIndex:endIndex]

    # 
    fo = open(outFile, "wb")
    s = struct.Struct('<L')

    data = s.pack(segmentDataLen)
    DataBuff = data + DataBuff;

    checksum = 0xA50615A5;
    for index in range(0, len(DataBuff) ,4):
        itemData = DataBuff[index:(index+4)]
        itemValue = s.unpack(itemData)[0]
        # print itemData, itemValue
        fo.write(itemData) 
        checksum ^= itemValue;

    # print checksum
    data = s.pack(checksum)
    fo.write(data) 
    fo.close()



def main():
    ret = 0
    retInfo ='OK'

    parser = argparse.ArgumentParser()
    # parser.add_argument()
    parser.add_argument("usrdir")
    args = parser.parse_args()

    # print args.usrdir

    UserDir = sys.path[0] + '/'
    # print UserDir

    usrFileDir = UserDir + 'code/'
    usrFile = usrFileDir + args.usrdir + '/' + 'userfunc.cpp'



    if os.path.exists(usrFile):
        makeCmd = 'make all -C ' + UserDir + ' USER_PATH=' + args.usrdir
        # print makeCmd
        (status, output) = commands.getstatusoutput(makeCmd)
        # print status, output

        if 0 == status:
            inFile = usrFileDir + args.usrdir + '/' + RAW_BIN_FILE
            outFile = usrFileDir + args.usrdir + '/' + BIN_FILE
            postFile(inFile, outFile)
            ret = 0
        else:
            logging.error(output)
            retInfo = "Failed!"
            ret = 1
    else:
        retInfo = "file Not exist!"
        ret = 2

    nowTime=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')    
    infoStr = nowTime + ' Compile ' + args.usrdir + ' ' + retInfo
    logging.info(infoStr)

    return ret


if __name__ == '__main__':

    log_file = "./makepatchlogger.log"
    logging.basicConfig(filename = log_file, level = logging.DEBUG)

    rslt = main()
