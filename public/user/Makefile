#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

.PHONY: all clean

help:
	@echo "make all - Build app, bootloader, partition table"
	@echo "make clean - Remove all build output"



# set by user
SERVER = 1
ifdef SERVER
BASEPRJ_PATH ?=  /var/tokymaker/esp/esp32prj_a/toky_main
IDF_PATH := /var/tokymaker/esp/esp32prj_b/esp-idf
# GCC_PATH := /var/tokymaker/esp/xtensa-esp32-elf/bin/
GCC_PATH := 

else
BASEPRJ_PATH ?= /Users/leoyan/SoftwarePrj/Sever/tokyIde/idf_tokylabs
IDF_PATH := /Users/leoyan/SoftwarePrj/SDK/esp-idf-v3.0
GCC_PATH := /Users/leoyan/SoftwarePrj/toolchains/xtensa-esp32-elf/bin/
endif

# 
PROJECT_NAME := patch
PROJECT_PATH := .


USER_PATH ?= user1

SDKCONFIG_MAKEFILE ?= $(BASEPRJ_PATH)/build/include/config/auto.conf
-include $(SDKCONFIG_MAKEFILE)

# The directory where we put all objects/libraries/binaries. The project Makefile can
# configure this if needed.
ifndef BUILD_DIR_BASE
BUILD_DIR_BASE := $(PROJECT_PATH)/code/$(USER_PATH)
endif
export BUILD_DIR_BASE

SRCFILE = $(BUILD_DIR_BASE)/userfunc.cpp
OBJFILE = $(BUILD_DIR_BASE)/userfunc.o 

BASE_SYM = base.sym


EXTRA_CPPFLAGS := -DESP32 -DARDUINO=101


CONFIG_TOOLPREFIX="xtensa-esp32-elf-"



OS ?=

# make IDF_PATH a "real" absolute path
# * works around the case where a shell character is embedded in the environment variable value.
# * changes Windows-style C:/blah/ paths to MSYS/Cygwin style /c/blah
ifeq ("$(OS)","Windows_NT")
# On Windows MSYS2, make wildcard function returns empty string for paths of form /xyz
# where /xyz is a directory inside the MSYS root - so we don't use it.
SANITISED_IDF_PATH:=$(realpath $(IDF_PATH))
else
SANITISED_IDF_PATH:=$(realpath $(wildcard $(IDF_PATH)))
endif

export IDF_PATH := $(SANITISED_IDF_PATH)

ifndef IDF_PATH
$(error IDF_PATH variable is not set to a valid directory.)
endif

ifneq ("$(IDF_PATH)","$(SANITISED_IDF_PATH)")
# implies IDF_PATH was overriden on make command line.
# Due to the way make manages variables, this is hard to account for
#
# if you see this error, do the shell expansion in the shell ie
# make IDF_PATH=~/blah not make IDF_PATH="~/blah"
$(error If IDF_PATH is overriden on command line, it must be an absolute path with no embedded shell special characters)
endif

ifneq ("$(IDF_PATH)","$(subst :,,$(IDF_PATH))")
$(error IDF_PATH cannot contain colons. If overriding IDF_PATH on Windows, use Cygwin-style /c/dir instead of C:/dir)
endif

# disable built-in make rules, makes debugging saner
MAKEFLAGS_OLD := $(MAKEFLAGS)
MAKEFLAGS +=-rR

# Default path to the project: we assume the Makefile including this file
# is in the project directory

COMPONENT_LDFLAGS :=


INC =  -I./
INC += -I./build/include
INC += -I$(BASEPRJ_PATH)/main
INC += -I$(BASEPRJ_PATH)/components/esp32_digital_led_lib/include
INC += -I$(BASEPRJ_PATH)/components/AdafruitScreen/include
INC += -I$(BASEPRJ_PATH)/components/AdafruitScreen/Adafruit_GFX
INC += -I$(BASEPRJ_PATH)/components/arduino/cores/esp32
INC += -I$(IDF_PATH)/components/soc/esp32/include
INC += -I$(IDF_PATH)/components/esp32/include
INC += -I$(IDF_PATH)/components/driver/include
INC += -I$(IDF_PATH)/components/freertos/include
INC += -I$(IDF_PATH)/components/heap/include
INC += -I$(IDF_PATH)/components/soc/include
INC += -I$(IDF_PATH)/components/log/include


$(info, $(PROJECT_PATH))





$(info, $(BUILD_DIR_BASE))

# Set variables common to both project & component
include $(IDF_PATH)/make/common.mk

all:


IDF_VER := $(shell cd ${IDF_PATH} && git describe --always --tags --dirty)

# Set default LDFLAGS
EXTRA_LDFLAGS ?=
LDFLAGS ?= -nostdlib \
	$(EXTRA_LDFLAGS) \
	-Wl,--gc-sections	\
	-Wl,-static	\
	-Wl,--start-group	\
	$(COMPONENT_LDFLAGS) \
	-T esp32.userfunc.ld \
	-T $(IDF_PATH)/components/esp32/ld/esp32.rom.ld \
	-Wl,--just-symbols=$(BASE_SYM) \
	-lgcc \
	-lstdc++ \
	-lgcov \
	-Wl,--end-group \
	-Wl,-EL


# Set default CPPFLAGS, CFLAGS, CXXFLAGS
# These are exported so that components can use them when compiling.
# If you need your component to add CFLAGS/etc for it's own source compilation only, set CFLAGS += in your component's Makefile.
# If you need your component to add CFLAGS/etc globally for all source
#  files, set CFLAGS += in your component's Makefile.projbuild
# If you need to set CFLAGS/CPPFLAGS/CXXFLAGS at project level, set them in application Makefile
#  before including project.mk. Default flags will be added before the ones provided in application Makefile.

# CPPFLAGS used by C preprocessor
# If any flags are defined in application Makefile, add them at the end. 
CPPFLAGS ?=
EXTRA_CPPFLAGS ?=
CPPFLAGS := -DESP_PLATFORM -D IDF_VER=\"$(IDF_VER)\" -MMD -MP $(CPPFLAGS) $(EXTRA_CPPFLAGS)

# Warnings-related flags relevant both for C and C++
COMMON_WARNING_FLAGS = -Wall -Werror=all \
	-Wno-error=unused-function \
	-Wno-error=unused-but-set-variable \
	-Wno-error=unused-variable \
	-Wno-error=unused-value \
	-Wno-error=unused-function \
	-Wno-error=unused-parameter \
	-Wno-error=deprecated-declarations \
	-Wextra \
	-Wno-unused-parameter -Wno-sign-compare

# Flags which control code generation and dependency generation, both for C and C++
COMMON_FLAGS = \
	-ffunction-sections -fdata-sections \
	-fstrict-volatile-bitfields \
	-mlongcalls \
	-nostdlib \
	-fstack-protector \
	-nostartfiles

#  it must be same with,  project.mk of IDF  (config.h)
# Optimization flags are set based on menuconfig choice
ifdef CONFIG_OPTIMIZATION_LEVEL_RELEASE
OPTIMIZATION_FLAGS = -O1
else
OPTIMIZATION_FLAGS = -Og
endif

ifdef CONFIG_OPTIMIZATION_ASSERTIONS_DISABLED
CPPFLAGS += -DNDEBUG
endif


# Enable generation of debugging symbols
# (we generate even in Release mode, as this has no impact on final binary size.)
DEBUG_FLAGS ?= -ggdb

# List of flags to pass to C compiler
# If any flags are defined in application Makefile, add them at the end.
CFLAGS ?=
EXTRA_CFLAGS ?=
CFLAGS := $(strip \
	-std=gnu99 \
	$(OPTIMIZATION_FLAGS) $(DEBUG_FLAGS) \
	$(COMMON_FLAGS) \
	$(COMMON_WARNING_FLAGS) -Wno-old-style-declaration \
	$(CFLAGS) \
	$(EXTRA_CFLAGS))

# List of flags to pass to C++ compiler
# If any flags are defined in application Makefile, add them at the end.
CXXFLAGS ?=
EXTRA_CXXFLAGS ?=
CXXFLAGS := $(strip \
	-std=gnu++11 \
	-fno-rtti \
	$(OPTIMIZATION_FLAGS) $(DEBUG_FLAGS) \
	$(COMMON_FLAGS) \
	$(COMMON_WARNING_FLAGS) \
	$(CXXFLAGS) \
	$(EXTRA_CXXFLAGS))

ifdef CONFIG_CXX_EXCEPTIONS
CXXFLAGS += -fexceptions
else
CXXFLAGS += -fno-exceptions
endif
export CFLAGS CPPFLAGS CXXFLAGS

# Set target compiler. Defaults to whatever the user has
# configured as prefix + ye olde gcc commands
CC := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))gcc
CXX := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))c++
LD := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))ld
AR := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))ar
OBJCOPY := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))objcopy
SIZE := $(GCC_PATH)$(call dequote,$(CONFIG_TOOLPREFIX))size
export CC CXX LD AR OBJCOPY SIZE

PYTHON=$(call dequote,$(CONFIG_PYTHON))


APP_ELF:=$(BUILD_DIR_BASE)/$(PROJECT_NAME).elf
APP_MAP:=$(APP_ELF:.elf=.map)
APP_BIN:=$(APP_ELF:.elf=.bin)
APP_BIN_UNSIGNED := $(APP_BIN:.bin=-raw.bin)


$(OBJFILE): $(SRCFILE) Makefile
	$(summary) CXX $(patsubst $(PWD)/%,%,$(CURDIR))/$@
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(INC) -c $< -o $@

$(APP_ELF): $(OBJFILE)
	$(summary) LD $(patsubst $(PWD)/%,%,$@)
	$(CC) $(LDFLAGS) $< -o $@ -Wl,-Map=$(APP_MAP)



OBJCOPY_EMBED_ARGS := -S -g -I binary -O elf32-xtensa-le --binary-architecture xtensa
$(APP_BIN): $(APP_ELF)
	$(summary) EMBED $(patsubst $(PWD)/%,%,$@)
	$(OBJCOPY) $(OBJCOPY_EMBED_ARGS) $< $@	


$(BUILD_DIR_BASE):
	mkdir -p $(BUILD_DIR_BASE)


###from Makefile.projbuild

ESPTOOLPY_SRC := $(IDF_PATH)/components/esptool_py/esptool/esptool.py
ESPTOOLPY := $(PYTHON) $(ESPTOOLPY_SRC) --chip esp32

# CONFIG_ESPTOOLPY_FLASHMODE = "dio"
# CONFIG_ESPTOOLPY_FLASHFREQ_80M = 1
# CONFIG_ESPTOOLPY_FLASHSIZE_4MB = 1


# ESPFLASHMODE ?= $(CONFIG_ESPTOOLPY_FLASHMODE)
# ESPFLASHFREQ ?= $(CONFIG_ESPTOOLPY_FLASHFREQ)
# ESPFLASHSIZE ?= $(CONFIG_ESPTOOLPY_FLASHSIZE)
#ESPTOOL_FLASH_OPTIONS := --flash_mode $(ESPFLASHMODE) --flash_freq $(ESPFLASHFREQ) --flash_size $(ESPFLASHSIZE)
ESPTOOL_FLASH_OPTIONS :=
ESPTOOL_ELF2IMAGE_OPTIONS :=


$(APP_BIN_UNSIGNED): $(APP_ELF) $(ESPTOOLPY_SRC)
	$(ESPTOOLPY) elf2image $(ESPTOOL_FLASH_OPTIONS) $(ESPTOOL_ELF2IMAGE_OPTIONS) -o $@ $<


all: $(APP_BIN_UNSIGNED)

clean: 
	rm -f $(APP_ELF) $(APP_BIN_UNSIGNED) $(APP_MAP)

flags:
	echo $(CPPFLAGS) 
	echo $(CXXFLAGS) 
	echo $(EXTRA_CXXFLAGS) 
	echo $(LDFLAGS)


