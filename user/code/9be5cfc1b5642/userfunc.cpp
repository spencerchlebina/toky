#include "gen_device.h" 

void User_func(void)
{
  int cnt=0;

  outAdd(23);
  outAdd(26);
  addButton(39);
  addButton(36);
  addTouchPad(12);
  addTouchPad(14);
  addTouchPad(13);
  outAddServo(25);

  startScreen(61);
  startButton();
  startTouchPad();

  cnt = 0;
  outWrite(23,90);
  outTone(26,4, (note_t)0);

  while(1){
    screenClr();
    if (isButtonClicked(39)) {
      consolePrint("KeyA");

    }
    if (isButtonPressing(36)) {
      consolePrint("KeyB");

    }
    if (isTouchPadClicked(12)) {
      consolePrint("T1");

    }
    if (isTouchPadClicked(14)) {
      consolePrint("T2");

    }
    if (isTouchPadPressing(13)) {
      consolePrint("T3");

    }
    cnt += 10;
    if (cnt > 100) {
      cnt = 0;

    }
    screenClr();
    screenPrint((String("IN1") + String(":") + String((inRead(34)))));
    screenUpdate();
    outWriteServo(25,cnt);
    delay(1000);
    delay(5);
  }
}