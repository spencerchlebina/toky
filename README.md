# To execute
- just `node index.js &` or `nodejs index.js &` in linux 
- **Note: Possibly look up nodemon package for development. You don't have to restart the ndoe server every time you want to see the effects of changes you made to the site.
- **Also, is jQuery actually being used? I d could not find where it is used in the code. The jQuery files may be redundant.


- Changes made by Spencer:
    - Added routes to the index.js file so data can be passed to the API endpoints (Ctrl+F search for "Changes Below" and "Changes Above")
    - For testing purposes, the site is set to run on localhost:3000
    - Added node modules (jsonwebtoken, config, bcrypt, mongoose/mongodb, joi, lodash)
    - Installed MongoDB
    - Changes made in public_b:
        - Added config, middleware, models, and routes folders
        - Created files to hold JWT Private Key for encryption in config folder
        - Created an authorization file(auth) in the middleware folder to check if requests are from authorized users and have not been tampered with
        - Created data models(models folder) for users(user), clicks(click), and projects(project) as well as validation functions for these requests
        - Created APIs(routes folder) for registration(users), logging in(auth), click tracking(clicks), and uploading projects(projects)
        - Created new Vues in main.js for the registration, logging in, logging off, and project upload capabilities (Ctrl+F search for "Changes Below" and "Changes Above")
        - Created logging function in main.js to log user click data (Ctrl+F search for "Changes Below" and "Changes Above")
        - Added div containers to the index.html file for the Vues added in main.js (Ctrl+F search for "Changes Below" and "Changes Above")
        - Added css styling for new Vues in my.css (Ctrl+F search for "Changes Below" and "Changes Above")
    - How it works:
        - The data model is defined in the models directory, the routes (and the GET, POST, PUSH, or DELETE request types for that route) are defined in the routes directory, and the authentication middleware is defined in the middleware directory
        - In order to implement the route which you can GET, POST, PUSH, or DELETE data to/from MongoDB, you must define the route/endpoint in the index.js file and create a way to send the data to the route/endpoint (I used XMLHttpRequest)
        - You must also define a handler for each request type (GET, POST, PUSH, or DELETE) in the route/endpoint file
        - If you want to create a new table in MongoDB, you must define a data model, define a route (and the allowable request types for the route), and possibly use the preexisting authentication middleware function or create a new one
        - Then, you must define the route/endpoint in the index.js file and when requests of the correct type are sent to the route/endpoint, it will be handled by the route and model files
        - It is a good idea to implement some form of data validation as well in order to be sure that you are receiving the correct data in the correct form
        - Use the preexisting routes and models as a framework to build your new routes and models and refer to the node.js documentation sections on MongoDB and the mongoose module
