//Including the required node modules
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Pointing to the location of the config files for the config module
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

//Creating the click schema/model
const clickSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        enum: ['signin', 'signout', 'signup', 'undo', 'redo', 'clear', 'save', 'upload', 'open', 'connect', 'compile']
    },
    userid: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true,
        default: Date.now
    }
});
//Creating the click model in MongoDB
const Click = mongoose.model('Click', clickSchema);
//Validation function for click data
function validateClick(click){
    //Creating the Joi click schema
    const schema = {
        name: Joi.string().min(4).max(255).required()
    };
    //Using the built-in Joi validate function to validate click data
    return Joi.validate(click, schema);
}

module.exports.Click = Click; //Exporting the click schema for use in other files
module.exports.validate = validateClick; //Exporting the valtidation function for use in other files