//Including the required node modules
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Pointing to the location of the config files for the config module
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

//Creating user schema/model
//Commented out sections are for later expansion of the user model. If implemented, remember to add them to the validateUser function at the end of the file.
const userSchema = new mongoose.Schema({
    /*firstName: {
        type: String,
        required: true,
        maxlength: 50
    },
    lastName: {
        type: String,
        required: true,
        maxlength: 50
    },*/
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024,
    }/*,
    goldCoins: {
        type: Number,
        required: true,
        default: 0
    },
    isPremium: {
        type: Boolean,
        required: true,
        default: false
    }*/
});

//Adding a method (generatAuthToken) to the userSchema for use in creating a JSON Web Token (JWT) for use in authenticating client requests
userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({_id: this._id, email: this.email}, config.get('jwtPrivateKey'));
    return token;
}
//Creating the user mdoel in MongoDB
const User = mongoose.model('User', userSchema);
//Function to validate the request data when loggin on or signing up
function validateUser(user){
    //Creating the Joi user schema
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()/*,
        firstName: Joi.string().max(50).required(),
        lastName: Joi.string().max(50).required(),
        goldCoins: Joi.number().required(),
        isPremium: Joi.boolean().required()*/
    };
    //Using the built-in Joi validate function to validate user data
    return Joi.validate(user, schema);
}

module.exports.User = User; //Exporting the user model for use in other files
module.exports.validate = validateUser; //Exporting teh validation function fo ruse in other files