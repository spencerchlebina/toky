//Including the required node modules
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Pointing to the location of the config files for the config module
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

//Creating the block schema for use in the project schema as an object
const blockSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    number: {
        type: Number,
        required: true
    }
});

//Creating the project schema/model. Commented out sections are for future expansion of teh project model. Remember to change the validation function at the end of the file as well.
const projectSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    url: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    title: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    xmlText: {
        type: String,
        required: true,
        minlength: 5
    },
    description: {
        type: String,
        required: true,
        minlength: 5
    },
    blocks: [blockSchema]/*,
    tags: [String]*/
});
//Creating the project model in MongoDB
const Project = mongoose.model('Project', projectSchema);
//Function to validate project request data
function validateProject(project){
    //Creating the Joi project schema
    const schema = {
        name: Joi.string().min(5).max(255).required(),
        url: Joi.string().min(5).max(255).required(),
        title: Joi.string().min(5).max(255).required(),
        description: Joi.string().min(5).required(),
        xmlText: Joi.string().min(5).required(),
        blocks: Joi.array().required()/*,
        tags: Joi.array()*/
    };
    //Using the built-in Joi validate function to validate project data
    return Joi.validate(project, schema);
}

module.exports.Project = Project; //Exporting the project model for use in other files
module.exports.validate = validateProject; //Exporting the validation fulction for use in other files