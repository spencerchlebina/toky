#include "gen_extern.h" 

void User_func(void)
{
  U16 _cnt_=0;
  float vdist=0;

  addButton(39);
  outAdd(23);
  addButton(36);

  startButton();
  startScreen(61);

  for(U16 _cnt_=0;;_cnt_++){
    vdist = (getHeadDistance());
    if (isButtonClicked(39)) {
      screenClr();
      screenPrint((String("La distance est de :") + String((float)(getHeadDistance()))));
      screenUpdate();
      outMute(23);

    } else if (isButtonClicked(36)) {
      if (vdist < 30) {
        outNote(23,7, (note_t)0);

      } else {
        outMute(23);

      }
    }
    delay(5);
  }
}