#include "gen_extern.h" 

void User_func(void)
{
  U16 _cnt_=0;
  float vsonar=0;
  float vcount=0;
  float vreset=0;

  addButton(36);
  addButton(39);

  startButton();
  startScreen(61);

  for(U16 _cnt_=0;;_cnt_++){
    if (isButtonClicked(36)) {
      vreset = 0;

    }
    if (isButtonClicked(39)) {
      vreset = 1;

    }
    if (vreset == 0) {
      vcount = 0;
      screenClr();
      screenPrint("start ?");
      screenUpdate();

    } else {
      vsonar = (getHeadDistance());
      if (vsonar <= 10) {
        vcount = vcount + 1;

      }
      screenClr();
      screenPrint((String("sonar ") + String((float)vsonar)));
      screenEnter();
      screenPrint((String("") + String((float))));
      screenUpdate();

    }
    delay(5);
  }
}