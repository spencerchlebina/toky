#include "gen_extern.h" 

void User_func(void)
{
  U16 _cnt_=0;
  float vd=0;

  startScreen(61);

  for(U16 _cnt_=0;;_cnt_++){
    vd = (getHeadDistance());
    if (vd >= 206) {
      screenClr();

    } else if (vd < 200 && vd >= 100) {
      screenSetTextSize(2);
      screenClr();
      screenPrint((String("Objet détecté") + String((float))));
      screenEnter();
      screenPrint((String() + String((float)vd)));
      screenUpdate();
    } else if (vd < 100 && vd >= 40) {
      screenClr();
      screenSetTextSize(2);
      screenPrint("Objet proche");
      screenEnter();
      screenPrint((String() + String((float)vd)));
      screenUpdate();
    } else if (vd < 40 && vd >= 0) {
      screenClr();
      screenSetTextSize(2);
      screenPrint("Danger");
      screenEnter();
      screenPrint((String() + String((float)vd)));
      screenUpdate();
    }
    delay(5);
  }
}