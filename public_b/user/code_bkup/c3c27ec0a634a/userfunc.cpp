#include "gen_extern.h" 

void User_func(void)
{
  U16 _cnt_=0;
  float vgaz=0;
  float vtemp=0;
  float vmov=0;
  float vtrue=0;
  float vtext_disp=0;
  float vclear_disp=0;
  float vblink=0;
  float vlight=0;
  float vposition=0;
  float vprob_duration=0;
  float valarm_auto=0;
  float valarm_auto_on=0;

  wifiStart("CMCC-vcEq","kt7ke6pd");
  mqttSetServer("io.adafruit.com",1883);
  mqttSetUsr("arwan_c","54fe1d092e35483090bd038ca77ccbee");
  mqttSetTopic("arwan_c/f","arwan_c/f");
  addButton(39);
  addTouchPad(14);
  addTouchPad(13);
  mqttAddSubscribe("alarm.light");
  addSerialRgb(18,1);
  mqttAddSubscribe("alarm.noise");
  outAdd(23);
  mqttAddSubscribe("alarm.open");
  outAddServo(25);
  mqttAddSubscribe("alarm.auto");

  mqttStart();
  startButton();
  startTouchPad();
  startSerialRgb();
  startScreen(61);

  vtext_disp = 650;
  vclear_disp = 350;
  vblink = 5;
  vposition = 100;
  valarm_auto_on = false;

  for(U16 _cnt_=0;;_cnt_++){
    if (isButtonPressing(39)) {
      vgaz = true;

    } else {
      vgaz = false;

    }
    if (isTouchPadPressing(14)) {
      vtemp = true;

    } else {
      vtemp = false;

    }
    if (isTouchPadPressing(13)) {
      vmov = true;

    } else {
      vmov = false;

    }
    if (timerAtEvery(0,10000)) {
      mqttPublish("alarm.gaz-level",vgaz);
      mqttPublish("alarm.inside-temperature-level",vtemp);
      mqttPublish("alarm.movement-rate",vmov);

    }
    if ((mqttGetData("alarm.light")) == true) {
      if (timerAtEvery(1,300)) {
        serialRgbSet(18,1,10,0,0);
        delay(500);
        serialRgbSet(18,1,0,0,0);
        delay(500);

      }

    } else {
      serialRgbSet(18,1,0,10,0);

    }
    if ((mqttGetData("alarm.noise")) == true) {
      outWrite(23,30);

    } else if (valarm_auto_on == false) {
      outMute(23);
    }
    if ((mqttGetData("alarm.open")) != vposition) {
      vposition = (mqttGetData("alarm.open"));
      outWriteServo(25,((360 / 100) * vposition));

    }
    if (vgaz == true) {
      for (U16 _cnt_ = 0; _cnt_ < vblink; _cnt_++) {
        arraySet(0,(U8)(1-1),((arrayGet(0,(U8)(1-1))) + 1));
        screenClr();
        screenSetTextSize(1);
        screenSetCursor(40,1);
        screenPrint("!WARNING!");
        screenEnter();
        screenEnter();
        screenPrint("Gaz level too high ");
        screenEnter();
        screenEnter();
        screenPrint("opening window");
        screenUpdate();
        delay(vtext_disp);
        screenClr();
        screenUpdate();
        delay(vclear_disp);
      }

    } else {
      arraySet(0,(U8)(1-1),0);

    }
    if (vtemp == true) {
      for (U16 _cnt_ = 0; _cnt_ < vblink; _cnt_++) {
        arraySet(0,(U8)(2-1),((arrayGet(0,(U8)(2-1))) + 1));
        screenClr();
        screenSetTextSize(1);
        screenSetCursor(40,1);
        screenPrint("!WARNING!");
        screenEnter();
        screenEnter();
        screenPrint("Indoor temp high");
        screenEnter();
        screenEnter();
        screenPrint("activating alarm now");
        screenUpdate();
        delay(vtext_disp);
        screenClr();
        screenUpdate();
        delay(vclear_disp);
      }

    } else {
      arraySet(0,(U8)(2-1),0);

    }
    if (vmov == true) {
      for (U16 _cnt_ = 0; _cnt_ < vblink; _cnt_++) {
        arraySet(0,(U8)(3-1),((arrayGet(0,(U8)(3-1))) + 1));
        screenClr();
        screenSetCursor(40,1);
        screenSetTextSize(1);
        screenPrint("!WARNING!");
        screenEnter();
        screenEnter();
        screenPrint("Window broken");
        screenEnter();
        screenEnter();
        screenPrint("activating alarm now");
        screenUpdate();
        delay(vtext_disp);
        screenClr();
        screenUpdate();
        delay(vclear_disp);
      }

    } else {
      arraySet(0,(U8)(3-1),0);

    }
    if ((mqttGetData("alarm.auto")) == "Automatic") {
      if ((arrayGet(0,(U8)(1-1))) > 10 || ((arrayGet(0,(U8)(2-1))) > 10 || (arrayGet(0,(U8)(3-1))) > 10)) {
        valarm_auto_on = true;
        outWrite(23,30);

      } else {
        valarm_auto_on = false;
        if ((mqttGetData("alarm.noise")) == false) {
          outMute(23);

        }

      }

    }
    delay(5);
  }

  true;
}