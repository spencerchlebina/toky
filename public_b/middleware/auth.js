//Including the required node modules
const jwt = require('jsonwebtoken');
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Pointing to the location of the config files for the config module
const config = require('config');

//User authentication function
function auth(req, res, next) {
    //Checking if a JWT was sent in the request headers under 'x-auth-token'
    const token = req.header('x-auth-token');
    if(!token) return res.status(401).send('Access denied. No token provided.');
    
    //Attempting to decode the JWT and setting req.user to the data in the JWT, or sending an error
    try{
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));
        req.user = decoded;
        next();
    }
    catch(err) {
        res.status(400).send('Invalid token.');
    }
}

module.exports = auth; //Exporting the authentication function for use in other files