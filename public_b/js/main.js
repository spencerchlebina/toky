const SERVER_URL = "https://create.tokylabs.com"
//const SERVER_URL = "http://localhost:8080"
const VERSION = "2.0"

var SVR_URL = "https://"+window.location.host;

//https://ourcodeworld.com/articles/read/202/how-to-include-and-use-jquery-in-electron-framework
// window.$ = window.jQuery = require('jquery');
// try {$ = jQuery = module.exports;} catch(e) {}

var outputArea = document.getElementById('output');
var runButton = document.getElementById('runButton');
var blocklyArea = document.getElementById('blocklyArea');
var blocklyDiv = document.getElementById('blocklyDiv');

//Creating the workspace and setting all the configurations. Refer to Google Blockly documentation.
var workspace = Blockly.inject(blocklyDiv,
    {   comments: true,
        toolbox: document.getElementById('toolbox'),
        collapse: true,
        maxBlocks: Infinity,
        media: 'media/',
        oneBasedIndex: true,
        scrollbars: true,
        grid:
             {spacing: 36,
              length: 3,
              colour: '#ddd',
              snap: true},
        zoom:
            {controls: true,
                wheel: false,
                startScale: 1.0,
                maxScale: 3,
                minScale: .25,
                scaleSpeed: 1.1},
        trashcan: true
    });

outputArea.value = "** " + VERSION +" **" + '\n';


function initApi(interpreter, scope) {
    // Add an API function for the alert() block, generated for "text_print" blocks.
    var wrapper = function(text) {
        text = text ? text.toString() : text===0 ? "0" : '';
        outputArea.value = outputArea.value + '\n' + text;
        // outputArea.scrollTop = outputArea.scrollHeight;
    };
    interpreter.setProperty(scope, 'alert',
        interpreter.createNativeFunction(wrapper));

    // Add an API function for the prompt() block.
    var wrapper = function(text) {
        text = text ? text.toString() : '';
        return interpreter.createPrimitive(prompt(text));
    };
    interpreter.setProperty(scope, 'prompt',
        interpreter.createNativeFunction(wrapper));
    
    initInterpreterGetInput(interpreter, scope);
    initInterpreterSetOutput(interpreter, scope);
    initInterpreterScreenPrint(interpreter, scope);
    initInterpreterScreenClear(interpreter, scope);
    initInterpreterScreenDrawPixel(interpreter, scope);
    initInterpreterDelay(interpreter, scope);

    // Add an API function for highlighting blocks.
    var wrapper = function(id) {
        id = id ? id.toString() : '';
        return interpreter.createPrimitive(highlightBlock(id));
    };

    interpreter.setProperty(scope, 'highlightBlock',
        interpreter.createNativeFunction(wrapper));

}
/**function**/



var highlightPause = false;
var latestCode = '';

function highlightBlock(id) {
    workspace.highlightBlock(id);
    highlightPause = true;
}

function resetStepUi(clearOutput) {
    workspace.highlightBlock(null);
    highlightPause = false;
    runButton.disabled = '';

    if (clearOutput) {
        outputArea.value = `Console`
    }
}





/**
 * Backup code blocks to localStorage.
 */
function backup_blocks() {
    if ('localStorage' in window) {
        var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
        window.localStorage.setItem('arduino', Blockly.Xml.domToText(xml));
    }
}

/**
 * Restore code blocks from localStorage.
 */
function restore_blocks() {
    if ('localStorage' in window && window.localStorage.arduino) {
        var xml = Blockly.Xml.textToDom(window.localStorage.arduino);
        Blockly.Xml.domToWorkspace(xml, Blockly.mainWorkspace);
    }
}

/**
 * Save Arduino generated code to local file.
 */
function saveCode() {
    var fileName = vex.dialog.prompt({
        message: 'What would you like to name your file?',
        placeholder: 'Tokymaker Project',
        callback: function (filename) {
            if(fileName){
                var blob = new Blob([Blockly.Arduino.workspaceToCode()], {type: 'text/plain;charset=utf-8'});
                saveAs(blob, fileName + '.js');
            }
        }
    })
}



/**
 * Save blocks to local file.
 * better include Blob and FileSaver for browser compatibility
 */
function save() {
    logging('save');
    var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var data = Blockly.Xml.domToText(xml);

    vex.dialog.prompt({
        message: 'What would you like to name your file?',
        placeholder: 'Tokymaker Project',
        callback: function (fileName) {

            if(fileName)
            {
                var blob = new Blob([data], {type: 'text/xml'});
                saveAs(blob, fileName + ".xml");                
            }
        }
    })

}

// Changes Below

//Removing the authentication token from localStorage on page load. This requires users to log back on if they close the page and reopen it.
window.onbeforeunload = function() {
    localStorage.removeItem('tokyn');
    return;
};

//Vue for uploading projects. It creates a pop-up modal form.
var uploadProjectVue = new Vue({
    data: {
        showModal: false,
        fullname: '',
        url: '',
        title: '',
        description: '',
        blocks: ''
    },
    el: '#projectUploadVueContainer',
    template: `
    <div @click="controlProjectUpload">

        <div class="item" id="login" style=" margin-right: 3px;
        padding-right: 3px;" title="upload project">
            <i class="fa fa-cloud-upload"  id="myBtn" aria-hidden="true" ></i>
        </div>

        <div v-show="showModal" class="modal">
            <div class="inner-project-modal" @click.stop>
                <h1>Upload Project</h1>
                <form id="loginform" name="loginform" v-on:submit.prevent="handleSubmit3">
                    <label for="fullname">Name:</label>
                    <input type="text" name="fullname" id="fullname" class="txtfield" v-model="fullname" tabindex="1">
                    <br><br>
                    <label for="url">URL:</label>
                    <input type="text" name="url" id="url" class="txtfield" v-model="url" tabindex="2">
                    <br><br>
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title" class="txtfield" v-model="title" tabindex="3">
                    <br><br>
                    <label for="description">Description:</label>
                    <textarea rows="10" name="description" id="description" class="textarea" placeholder="Description" v-model="description" tabindex="4"></textarea>
                    <button type="submit" id="projectbtn" tabindex="5">Submit</button>
                </form>
            </div>
        </div>

    </div>
    `,
    methods: {
        //Function to display and hide the project upload form
        controlProjectUpload: function() {
            if(!this.showModal){
                this.showModal = true;
            } else{
                this.closeProjectUpload();
            }
        },
        //Function to close the project upload form and clear its contents
        closeProjectUpload: function() {
            this.showModal = false;
            this.fullname = '';
            this.url = '';
            this.title = '';
            this.description = '';
        },
        //Function to handle submission of projects to MongoDB
        handleSubmit3: function() {
            if (fullname._value == '' || url._value == '' || title._value == '' || description._value == '') { //Checking if required fields are empty
                vex.dialog.alert('Please fill in all fields.');
                return;
            } else if (!localStorage.getItem('tokyn')) { //Checking if user is logged in
                vex.dialog.alert('You must sign in to upload a project.');
                return;
            }
            logging('upload');
            
            //Collecting project block data
            var projectXml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
            var projectData = Blockly.Xml.domToText(projectXml);
            var blocksArray = [];
            //Looping through the text version of the XML for the project to add the name of each to an array
            for(i = 0; i < Blockly.mainWorkspace.getAllBlocks().length; i++) {
                var frontIndex = projectData.indexOf('block type=') + 12;
                var backIndex = projectData.indexOf('"', frontIndex);
                var blok = projectData.slice(frontIndex, backIndex);
                projectData = projectData.slice(frontIndex);
                blocksArray.push(blok);
            }
            blocksArray.sort();
            var blocksObjectArray = [];
            //Looping through the array created above to combine blocks which occur more tha once into one objectwith a name and number property
            //blocksObjectArray is an array of objects of the form [{name: 'example', number: 2}, {name: 'another_example', number 5}, ...]
            for(i = 0; i < blocksArray.length; i++) {
                if(blocksArray[i] == blocksArray[i-1]){
                    blocksObjectArray[blocksObjectArray.length-1].number++;
                } else {
                    blocksObjectArray.push({
                        name: blocksArray[i],
                        number: 1
                    });
                }
            }
            //Creating a string variable that is the text representation of the XML for the project
            var xmlText = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace));
            
            //Sending the project data to MongoDB using XMLHttpRequest()
            let proj = new XMLHttpRequest();
            proj.open("POST", '/api/projects', true); //Setting the request type as POST and pointing to the /api/projects endpoint defined in index.js
            proj.setRequestHeader('Content-Type', 'application/json'); //The request will contain JSON data 
            proj.setRequestHeader('x-auth-token', localStorage.getItem('tokyn')); //Setting a custom header for authentication
            proj.send(JSON.stringify({
                name: fullname._value,
                url: url._value,
                title: title._value,
                description: description._value,
                xmlText: xmlText,
                blocks: blocksObjectArray
            }));
            this.closeProjectUpload(); //Closing project form after submission
        }
    }
});

//Vue for logging in. It creates a pop-up modal form.
var loginVue = new Vue({
    data : {
        showModal: false,
        email: '',
        password: ''
    },
    el : '#loginVueContainer',
    template : `
    <div @click="controlLogin">

        <div class="item" id="login" title="sign in" style=" margin-right: 1px;
        padding-right: 1px;">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
        </div>

        <div v-show="showModal" class="modal">
            <div class="inner-modal" @click.stop>
                <h1>Sign In</h1>
                <form id="loginform" name="loginform" v-on:submit.prevent="handleSubmit">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" class="txtfield" v-model="email" tabindex="1">
                    <br><br>
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="txtfield" v-model="password" tabindex="2">
                    <br>
                    <button type="submit" id="loginbtn" tabindex="3">Submit</button>
                </form>
            </div>
        </div>

    </div>
    `,
    methods: {
        //Function to display and hide the login form
        controlLogin: function() {
            if(!this.showModal){
                this.showModal = true;
            } else{
                this.closeLogin();
            }
        },
        //Function to close the login form
        closeLogin: function() {
            this.showModal = false;
            this.email = '';
            this.password = '';
        },
        //Function to handle submission of user data (email and password) to MongoDB
        handleSubmit: function() {
            if (email._value == '' || password._value == ''){ //Checking if required fields are empty
                vex.dialog.alert('Enter a valid email and password.');
                return;
            } else if (localStorage.getItem('tokyn')) { //Checking if user is already logged on
                vex.dialog.alert('You are already signed in.');
                return;
            }
            //Sending the user data to MongoDB using XMLHttpRequest()
            let xhr = new XMLHttpRequest();
            xhr.open("POST", '/api/auth', true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify({
                email: email._value,
                password: password._value
            }));
            //Checking for the authentication token in the response header and saving it in local storage
            xhr.onreadystatechange = function() {
                if(this.readyState == this.HEADERS_RECEIVED) {
                    var tokyn = xhr.getResponseHeader("x-auth-token");
                    if (!tokyn) {
                        vex.dialog.alert('No Response Token Received.');
                    } else {
                        localStorage.setItem('tokyn', tokyn);
                        logging('signin');
                        vex.dialog.alert('You are signed in!');
                    }
                }
            }
            this.closeLogin(); //Closing the login form after submission
        }
    }
});

//Vue for signing out. It only shows the signout button.
var signoutVue = new Vue({
    el : "#signoutVueContainer",
    template : `
    <div @click="controlSignout">

        <div class="item" id="signout" title="sign out" style=" margin-right: 3px;
        padding-right: 3px;">
            <i class="fa fa-user-times" aria-hidden="true"></i>
        </div>
    
    </div>
    `,
    methods: {
        //Function to log the user offby removing the authentication token from local storage or alert the user that they cannot log out because they are not logged in
        controlSignout: function() {
            if(localStorage.getItem('tokyn')){
                logging('signout');
                localStorage.removeItem("tokyn");
                vex.dialog.alert('You have been signed out.');
                return;
            } else {
                vex.dialog.alert('You are not signed in.');
                return;
            }
        }
    }
})

//Vue to register a new account. It creates a popup modal form. If you want to add fields for the user to enter upon registration, add the fields to the data section and the HTML template below.
var registerVue = new Vue({
    data : {
        showModal: false,
        eml: '',
        pass: ''
    },
    el : '#registerVueContainer',
    template : `
    <div @click="controlRegister">

        <div class="item" id="login" title="register" style=" margin-right: 2px;
        padding-right: 2px;">
            <i class="fa fa-user-plus" aria-hidden="true"></i>
        </div>

        <div v-show="showModal" class="modal">
            <div class="inner-modal" @click.stop>
                <h1>Register</h1>
                <form id="loginform" name="loginform" v-on:submit.prevent="handleSubmit2">
                    <label for="eml">Email:</label>
                    <input type="text" name="eml" id="eml" class="txtfield" v-model="eml" tabindex="1">
                    <br><br>
                    <label for="pass">Password:</label>
                    <input type="password" name="pass" id="pass" class="txtfield" v-model="pass" tabindex="2">
                    <br>
                    <button type="submit" id="loginbtn" tabindex="3">Submit</button>
                </form>
            </div>
        </div>

    </div>
    `,
    methods: {
        //Function to control the display of the registration modal form
        controlRegister: function() {
            if(!this.showModal){
                this.showModal = true;
            } else{
                this.closeRegister();
            }
        },
        //Function to close the registration modal form
        closeRegister: function() {
            this.showModal = false;
            this.eml = '';
            this.pass = '';
        },
        //Function to handle submission of the registration data to MongoDB
        handleSubmit2: function() {
            //Check if the required fields are filled out and if the user is already logged in
            if (eml._value == '' || pass._value == ''){
                vex.dialog.alert('Enter a valid email and password.');
                return;
            } else if (localStorage.getItem('tokyn')) {
                vex.dialog.alert('You are already signed in.');
                return;
            }
            //Sending the registration data to MongoDB using XMLHttpRequest()
            let abc = new XMLHttpRequest();
            abc.open("POST", '/api/users', true);
            abc.setRequestHeader('Content-Type', 'application/json');
            abc.send(JSON.stringify({
                email: eml._value,
                password: pass._value
            }));
            //Checking for the authentication token in the response header and saving it in local storage, unless the user is already registered
            abc.onreadystatechange = function() {
                if(this.readyState == this.HEADERS_RECEIVED) {
                    var tokyn = abc.getResponseHeader("x-auth-token");
                    if (!tokyn && this.status != 400) {
                        vex.dialog.alert('No response token received. Failed to regster user.');
                        return;
                    } else if(!tokyn && this.status == 400) {
                        vex.dialog.alert('User already registered.');
                        return;
                    } else {
                        localStorage.setItem('tokyn', tokyn);
                        logging('signup');
                        vex.dialog.alert('You are signed up and signed in.');
                    }
                }
            }
            this.closeRegister(); //Hide the registrational modal form
        }
    }
});

//Function to keep track of user clicks. If you want to add logging to a new button, add this function and pass the name you would like to call that click as a string argument.
//You must also add the name of the click to the click model in models/click in the 'enum' section of the click model.
function logging(name) {
    let click = new XMLHttpRequest();
            click.open("POST", '/api/clicks', true);
            click.setRequestHeader('Content-Type', 'application/json');
            click.setRequestHeader('x-auth-token', localStorage.getItem('tokyn'));
            click.send(JSON.stringify({
                name: name
            }));
}

//Changes Above

/**
 * Load blocks from local file.
 */

function load(event) {
    var files = event.target.files;
    // Only allow uploading one file.
    if (files.length != 1) {
        return;
    }


    // FileReader
    var reader = new FileReader();
    reader.onloadend = function(event) {
        var target = event.target;
        // 2 == FileReader.DONE
        if (target.readyState == 2) {
            try {
                var xml = Blockly.Xml.textToDom(target.result);
            } catch (e) {
                vex.dialog.alert('Error parsing XML:' + e);
                return;
            }
            var count = Blockly.mainWorkspace.getAllBlocks().length;

            if (count)
            {

                vex.dialog.confirm({
                    message: 'Replace or Merge existing blocks?',
                    buttons: [
                      $.extend({}, vex.dialog.buttons.YES, { text: 'Replace' }), 
                      $.extend({}, vex.dialog.buttons.NO, { text: 'Merge' })  
                    ],                    
                    callback: function (value) {
                        if (value)
                        {
                            Blockly.mainWorkspace.clear();
                        }

                        Blockly.Xml.domToWorkspace(xml, Blockly.mainWorkspace);
                    }
                })

            }
            else
            {
                Blockly.Xml.domToWorkspace(xml, Blockly.mainWorkspace);
            }

        }
        // Reset value of input after loading because Chrome will not fire
        // a 'change' event if the same file is loaded again.
        document.getElementById('load').value = '';
    };
    reader.readAsText(files[0]);
}

/**
 * Discard all blocks from the workspace.
 */
function discard() {
    logging('clear');
    var count = Blockly.mainWorkspace.getAllBlocks().length;

    if ( count < 2)
    {
        Blockly.mainWorkspace.clear();        
    }
    else
    {
        vex.dialog.confirm({
            message: 'Delete all blocks?',
            callback: function (value) {
                if (value)
                {
                    Blockly.mainWorkspace.clear();
                }
            }
        })
    }
}

function undo() {
    logging('undo');
    Blockly.mainWorkspace.undo(false);
}

function redo() {
    logging('redo');
    Blockly.mainWorkspace.undo(true);
}

/*
 * auto save and restore blocks
 */
function auto_save_and_restore_blocks() {
    // Restore saved blocks in a separate thread so that subsequent
    // initialization is not affected from a failed load.
    window.setTimeout(restore_blocks, 0);
    // Hook a save function onto unload.
    bindEvent(window, 'unload', backup_blocks);
    //tabClick(selected);

    // Init load event.
    var loadInput = document.getElementById('load');
    loadInput.addEventListener('change', load, false);
    document.getElementById('fakeload').onclick = function() {
        logging('open');
        loadInput.click();
    };
}

/**
 * Bind an event to a function call.
 * @param {!Element} element Element upon which to listen.
 * @param {string} name Event name to listen to (e.g. 'mousedown').
 * @param {!Function} func Function to call when event is triggered.
 *     W3 browsers will call the function with the event object as a parameter,
 *     MSIE will not.
 */
function bindEvent(element, name, func) {
    if (element.addEventListener) {  // W3C
        element.addEventListener(name, func, false);
    } else if (element.attachEvent) {  // IE
        element.attachEvent('on' + name, func);
    }
}



/*JS二进制 https://www.cnblogs.com/SolarWings/p/6262932.html*/

/**offline
 1 byte: Frame head = command
 2 bytes: the length of Frame content
 2 bytes: the ~length of Frame content
 n bytes: Frame content
 **/

function makeLoadCodeFrame(codeData){
    const TRANSCATION_BASE = 0xF0;
    const TRANSCATION_LOADCODE = (TRANSCATION_BASE+1);
    var headerLen = 5;

    codeLength = codeData.length;
    FrameBuffer = new ArrayBuffer(headerLen + codeLength);

    var Frame = new DataView(FrameBuffer);
    var FrameU8 = new Uint8Array(FrameBuffer);
    var FrameCode = new Uint8Array(FrameBuffer, headerLen);


    index  = 0;
    Frame.setUint8(index, TRANSCATION_LOADCODE);
    index++;
    Frame.setUint16(index, codeLength, false);
    index += 2;
    Frame.setUint16(index, ~codeLength, false);

    FrameCode.set(codeData);

    return new Uint8Array(FrameBuffer);
}


var runBtnClass = document.querySelector("#runButton i");



/**getcode and send**/
var BLE_sendIndex;
var BLE_sendBuffer = new Uint8Array();

var getInfoIntervalHandle = null;
var isBleRead = false;
window.isUploading = false;

function upload(){
//Changes Below
    if (!localStorage.getItem("tokyn")) {
        vex.dialog.alert('Sign In or Sign Up to Use.' );
        return;
    }
    
    logging('compile');
    
//Changes Above

    if (window.isUploading)
    {
        return;
    }

    runScript = Blockly.Arduino.workspaceToCode(workspace);

    // console.log("script length=",runScript.length, runScript[1]);


    runButton.disabled = true; //todo no effect


    if (!bleVue.$data.cachedBLEcharacteristic)
    {
        vex.dialog.alert('Connect BLE first!' );
        return;
    }

    window.isUploading = true;

    outputArea.value = 'Compiling...';

    // sendToBluetoothDevice();
    // ref:  https://segmentfault.com/a/1190000004322487
    $.get(
        SVR_URL + "/sendCode/",
        {code : runScript},
        function(hexa){
            // console.log("Id of compiled code " + hexa);
            try{

                var request = new XMLHttpRequest();
                request.open("GET", SVR_URL + "/getHex/"+hexa, true);
                request.responseType = "arraybuffer";


                request.onload = function() {

                    if(this.status == 200||this.status == 304){

                        var arrayBuffer = request.response;
                        codeData = new Uint8Array(arrayBuffer);

                        BLE_sendBuffer = makeLoadCodeFrame(codeData);

                        BLE_sendIndex = 0;
                        return new Promise(function(resolve, reject) {
                            sendNextDataBatch(resolve, reject);
                        });

                    }
                    else{
                        outputArea.value = 'Compilation Failed!';
                        window.isUploading = false;
                    }

                };

                request.onerror = function (err) {
                    // console.error(err);
                    outputArea.value = 'Compilation Failed!';
                    window.isUploading = false;
                };

                request.send();
            }
            catch(err){
                // console.error(err);
                outputArea.value = 'Compilation Failed!';
                window.isUploading = false;
            }
        }
    ).fail(function() {
       window.isUploading = false;
       outputArea.value = 'Get URL failed!';
    });


}




function sendNextDataBatch(resolve, reject) {
    // Can only write 512 bytes at a time to the characteristic
    // Need to send the image data in 512 byte batches
    if (BLE_sendIndex + 256 < BLE_sendBuffer.length) {
        bleVue.$data.cachedBLEcharacteristic
        .writeValue(BLE_sendBuffer.slice(BLE_sendIndex, BLE_sendIndex + 256))
        .then(() =>{
            BLE_sendIndex += 256;
            // console.log("Data Transfer Progress : " + Math.floor(BLE_sendIndex*1000.0/BLE_sendBuffer.length)/10.0 + "%");
            outputArea.value = 'Send ' + Math.floor(BLE_sendIndex*1000.0/BLE_sendBuffer.length)/10.0 +'%';
            sendNextDataBatch(resolve, reject);
        })
        .catch(error => {reject(error);
            outputArea.value = 'BLE error';
            window.isUploading = false;
        });
    }
    else {
        // Send the last bytes
        var result='100%';
        if(BLE_sendIndex < BLE_sendBuffer.length) {
            bleVue.$data.cachedBLEcharacteristic
            .writeValue(BLE_sendBuffer.slice(BLE_sendIndex, BLE_sendBuffer.length))
            .then(resolve)
            .catch(error => {reject(error); result = 'BLE error'; });
        }
        else {
            resolve();
        }

        outputArea.value = result;
        window.isUploading = false;
    }
}


// deal replace block.  or there are many isolate blocks on workspace.

var preIsolateEvent = null;

function postChange(event) {
    // console.log(event);
    // console.log(event.type);

    if (event.type == Blockly.Events.MOVE) {
        if ( (typeof(event.newParentId) != "undefined") && (typeof(event.oldParentId)=="undefined") ){
            //new event


            if ( null != preIsolateEvent ){

                if ( event.blockId != preIsolateEvent.blockId
                    && event.newParentId == preIsolateEvent.oldParentId
                    && event.newInputName == preIsolateEvent.oldInputName ){

                    var isolatedBlock = Blockly.mainWorkspace.getBlockById(preIsolateEvent.blockId);
                    isolatedBlock.dispose();
                }
            }

            preIsolateEvent = null;
        }
        else if ( (typeof(event.newParentId) == "undefined") && (typeof(event.oldParentId)!="undefined") ) {
            //isolate event
            preIsolateEvent = event;
        }
        else {
            preIsolateEvent = null;
        }
    }
    else {
        preIsolateEvent = null;
    }

}


// window.librariesToAdd = "";

window.addEventListener('resize', onresize, false);
// Load the interpreter now, and upon future changes.
workspace.addChangeListener(function(event) {
    if (!(event instanceof Blockly.Events.Ui)) {
        // Something changed. Parser needs to be reloaded.
        // resetInterpreter();
    }
});


workspace.addChangeListener(postChange);

auto_save_and_restore_blocks();

// onresize();

function hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function addClass(ele,cls) {
    if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}

function removeClass(ele,cls) {
    if (hasClass(ele,cls)) {
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(reg,'');
    }
}

var isExpand = false;

onload = function () {
    Blockly.svgResize(workspace);
    var debugWin = document.getElementById("debug-win");
    var debugBtn = document.getElementById("debug-btn");
    var debugText = document.querySelector("#debug-win pre");
    debugBtn.onclick = function () {
        if (isExpand) {
            // debugWin.style.height = "40px"
            removeClass(debugWin, "expand");
            removeClass(debugBtn, "fa fa-hand-o-down");
            addClass(debugBtn, "fa fa-hand-o-up");

        } else {
            // debugWin.style.height = "50%"
            addClass(debugWin, "expand");
            removeClass(debugBtn, "fa fa-hand-o-up");
            addClass(debugBtn, "fa fa-hand-o-down");
        }
        isExpand = !isExpand;
    }
}


const SERVICE_UUID  = 0x00EE;
var decoder = new TextDecoder("utf-8");

var bleVue = new Vue({
    data : {
        connected : false,
        device : null,
        devicesAlreadyFound : [],  //{name: 'Tokymaker1', select: 1, rssi : 33},{name: 'Tokymaker2', select: 2, rssi : 33}
        cachedBLEcharacteristic : null,
        environment : "browser",
        showList : false,
        delayCnt : 0,
        bluetooth : null
    },
    el : "#bleVueContainer",
    template : `
    <div @click="controlBle()">
        
        <div class="item" id="pairBluetooth" title="connect to ESP">
            <i class="fa fa-bluetooth-b" aria-hidden="true" :class="{bleConnected : connected}"></i>
        </div>
        <div v-show="showList" class="deviceList">
            <span v-if="devicesAlreadyFound.length==0" @click.stop.prevent="selectDevice(-1)">No device found</span>
            <ul id="devices">
                <li id="deviceItem" v-for="(device, index) in devicesAlreadyFound" @click.stop.prevent="selectDevice(index)">
                    {{device.name}}
                </li>
            </ul>
        </div>

    </div>
    `,
    created(){
        if (window.process){
            this.environment = "electron"; ////////////////////////////// Cordova code would allow another environment.
        }
        window.setInterval(this.getInfoFromDevice,1000);

        if(this.environment == "browser"){
            this.bluetooth = navigator.bluetooth;   
        }
        else{
            var webbluetooth = require("webbluetooth");
            this.devicesAlreadyFound = [];
            this.bluetooth = new webbluetooth.Bluetooth({deviceFound : this.handleDeviceFound});
        }
    },
    methods : {
        onDisconnected : function(){
            console.log('BLE is disconnected.');
            this.cachedBLEcharacteristic = null;
            this.device = null;
            this.connected = false;
            document.getElementById("pairBluetooth").style.color = pairBleOriginalColor;

        },
        getInfoFromDevice : function(){
            if(this.connected){
                if(!window.isUploading){
                    this.delayCnt++;
                }
                else{
                    this.delayCnt = 0;
                }

                if (this.delayCnt > 1 && this.cachedBLEcharacteristic){
                    this.delayCnt = 2;
                    this.cachedBLEcharacteristic.readValue()
                    .then(value => {
                        var byteBuff = value.buffer;
                        var newInfoLen = byteBuff.byteLength;
                        if (newInfoLen > 0){
                            outputArea.value += decoder.decode(byteBuff);
                            outputArea.scrollTop = outputArea.scrollHeight;
                        }
                    })
                    .catch(err=>{
                        console.log(err);
                    });
                }
            }
        },
        controlBle(){
            if (!this.cachedBLEcharacteristic) {
                this.connectToDevice();
            }
            else
            {
                this.device.gatt.disconnect();
                this.onDisconnected();
            }
        },
        connectToDevice(){
            logging('connect');
            if(this.environment != "browser"){
                this.showList = true;
            }
            this.bluetooth.requestDevice({
                filters: [{
                    services: [SERVICE_UUID]
                }]
            })
            .then(device => {
                console.log('> Connected to ' + device.name);
                // device.name is usually of the form 'Tokymaker2.0-#########'
                if(device.name.substring(9,12) == VERSION){
                    this.device = device;
                    this.connected = true;
                    device.addEventListener('gattserverdisconnected', this.onDisconnected);
                    return device.gatt.connect();
                }
                else{
                    vex.dialog.alert("Your Firmware is too old! Please check www.tokylabs.com for the newest version")
                    return Promise.reject("OldFirmware");
                }
            })
            .then(server => server.getPrimaryService(SERVICE_UUID))
            .then(service=> service.getCharacteristics())
            .then(characteristics => {
                this.cachedBLEcharacteristic = characteristics[0];
                console.log("Connected to the sweeeet ESP32");
                document.getElementById("pairBluetooth").style.color = "rgba(58,178,199,1)";
            })
            .catch(err=>console.log(err));
        },
        handleDeviceFound(newDevice, confirmConnectFunction){
            var deviceInTheList = this.devicesAlreadyFound.filter(e=>newDevice.name==e.name)[0];
            console.log('deviceInTheList=' + deviceInTheList);
            if(deviceInTheList){
                deviceInTheList.rssi = newDevice.adData.rssi;
                deviceInTheList.select = confirmConnectFunction;
            }
            else{
                this.devicesAlreadyFound.push({ name: newDevice.name, select: confirmConnectFunction, rssi : newDevice.adData.rssi});                
            }
        },
        selectDevice(index){
            this.showList = false;

            if (index < 0) return;

            this.devicesAlreadyFound[index].select();
            this.devicesAlreadyFound = []; 
        }
    }
})


let pairBleOriginalColor = document.getElementById("pairBluetooth").style.color;
