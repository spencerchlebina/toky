//Including node modules below
const auth = require('../middleware/auth'); //Including the 'auth' user authentication file from middleware 
const jwt = require('jsonwebtoken');
process.env["NODE_CONFIG_DIR"] = "./public_b/config"; //Setting the path of the config files for the config module
const config = require('config'); 
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {User, validate} = require('../models/user') //Importing the User object and the validate function from the user model
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//Creating the POST route for the /api/users API endpoint
router.post('/', async (req, res) => {
    //Using the validate function (imported above) to validate the request body
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //Checking if the user already exists and throwing an error if they do. This endpoint is for registration, not signing in
    let user = await User.findOne({email: req.body.email});
    if(user) return res.status(400).send();
    //Creating new user object and encrypting the password
    user = new User(_.pick(req.body, ['email', 'password']));
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save(); //Saving the user object to MongoDB
    //Creating encrypted token using JWT
    const token = user.generateAuthToken();
    //Sending the token through headers to the client to use for later requests
    res.header('x-auth-token', token).send();
});
//Exporting this route for reference in other files
module.exports = router;