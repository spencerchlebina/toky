//Including the necessary node modules
const auth = require('../middleware/auth'); //Including the user authentication file 'auth' from middleware
const {Project, validate} = require('../models/project'); //Importing the Project object and the validate function from the project model
const {User} = require('../models/user'); //Importing the User object from the user model
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//Creating the POST route for the /api/projects endpoint using the user authentication file 'auth' included above
router.post('/', auth, async (req, res) => {
    //Validating the request body using the validate function from the project model
    const {error} = validate(req.body);
    if (error) return res.status(401).send(error.details[0].message);
    //Validating the user making the request using the User object and MongoDB
    let user = await User.findById(req.user._id);
    if(!user) return res.status(400).send();
    //Creating new Project object (req.user is set in the user authentication file referenced above)
    project = new Project({
        name: req.body.name,
        url: req.body.url,
        email: req.user.email,
        title: req.body.title,
        description: req.body.description,
        xmlText: req.body.xmlText,
        blocks: req.body.blocks
    });
    await project.save(); //Saving Project object to MongoDB
});
//Exporting the projects route for use in other files
module.exports = router;