//Including the necessary node modules
const Joi = require('joi');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {User} = require('../models/user'); //Importing the User object from the user model
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//Creating the POST route for the /api/auth endpoint (This endpoint is used for logging on)
router.post('/', async (req, res) => {
    //Validating the request body using the validate function defined below
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //Checking if the user object exists in MongoDB and throwing an error if not
    let user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send('Invalid email or password');
    //Checking the validity fo the password in the request body using bcrypt
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).send('Invalid email or password');
    //Creating an authorization token with the generateAuthToken method fo the user model
    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(); //Sending the authorization token in the response header to the user for later use in authenticating requests
});
//Function for validation of the request body
function validate(req){
    //Defining the user model schema
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    
    return Joi.validate(req, schema); //Using Joi's built-in validate function to validate the request
}
//Exporting the auth route for use in other files
module.exports = router;