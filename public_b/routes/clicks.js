//Including the necessary node modules
const auth = require('../middleware/auth'); //Importing the user authentication file 'auth' from middleware
const {Click, validate} = require('../models/click'); //Importing the Click object and validate functions from the click model
const {User} = require('../models/user'); //Importing the User object from the user model
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//Creating the POST route for the /api/click API endpoint using the user authentication 'auth' file
router.post('/', auth, async (req, res) => {
    //Validating the request body using the validate function imported above
    const {error} = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //Checking if the user making the request exists in MongoDB
    let user = await User.findById(req.user._id);
    if(!user) return res.status(400).send();
    //Creating a new click object using the req.body and req.user (req.user created in teh 'auth' file referenced above)
    click = new Click({
        name: req.body.name,
        userid: req.user._id
    });
    await click.save(); //Saving the click object to MongoDB
});
//Exporting the clicks route for use in other files
module.exports = router;